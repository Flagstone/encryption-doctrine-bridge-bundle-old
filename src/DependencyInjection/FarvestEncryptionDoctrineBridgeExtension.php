<?php
/** *****************************************************************************************************************
 *  FarvestEncryptionDoctrineBridgeExtension.php
 *  -----------------------------------------------------------------------------------------------------------------
 *  @copyright 2019 Farvest
 *  @author Emmanuel Grosdemange <emmanuel.grosdemange@farvest.com>
 *  -----------------------------------------------------------------------------------------------------------------
 *  Created: 2019/12/09
 *  ***************************************************************************************************************** */

namespace Farvest\EncryptionDoctrineBridgeBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Exception;

/** *****************************************************************************************************************
 *  Class FarvestEncryptionExtension
 *  -----------------------------------------------------------------------------------------------------------------
 *  Base class of the Farvest Encryption Doctrine Bridge Bundle project
 *  -----------------------------------------------------------------------------------------------------------------
 *  @package Farvest\BaseEncoderBundle
 *  ***************************************************************************************************************** */
class FarvestEncryptionDoctrineBridgeExtension extends Extension
{
    /** **************************************************************************************************************
     *  Load bundle resources
     *  --------------------------------------------------------------------------------------------------------------
     *  @param array $configs
     *  @param ContainerBuilder $container
     *  @throws Exception
     *  ************************************************************************************************************* */
    public function load(array $configs, ContainerBuilder $container)
    {
        $loader = new YamlFileLoader(
            $container,
            new FileLocator(__DIR__.'/../Resources/config')
        );
        $loader->load('services.yaml');
    }
}