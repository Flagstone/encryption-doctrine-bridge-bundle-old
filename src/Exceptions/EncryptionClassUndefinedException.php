<?php
/** *****************************************************************************************************************
 *  EncryptionClassUndefinedException.php
 *  *****************************************************************************************************************
 *  @copyright 2019 Farvest
 *  @author Emmanuel Grosdemange <emmanuel.grosdemange@farvest.com>
 *  *****************************************************************************************************************
 *  Created: 2019/12/10
 *  ***************************************************************************************************************** */

namespace Farvest\EncryptionDoctrineBridgeBundle\Exceptions;

use Exception;

/** *****************************************************************************************************************
 *  Class EncryptionClassUndefinedException
 *  ----------------------------------------------------------------------------------------------------------------
 *  Exception thrown when a Encryption class that must be used with this bundle is not defined (or find).
 *  ----------------------------------------------------------------------------------------------------------------
 *  @package Farvest\EncryptionDoctrineBridgeBundle\Exceptions
 *  ***************************************************************************************************************** */
class EncryptionClassUndefinedException extends Exception
{

}