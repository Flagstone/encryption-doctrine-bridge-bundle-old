<?php
/** *****************************************************************************************************************
 *  EncryptionForm.php
 *  *****************************************************************************************************************
 *  @copyright 2019 Farvest
 *  @author Emmanuel Grosdemange <emmanuel.grosdemange@farvest.com>
 *  *****************************************************************************************************************
 *  Created: 2019/12/13
 ******************************************************************************************************************** */

namespace Farvest\EncryptionDoctrineBridgeBundle\EncryptionDoctrineBridge;

use Farvest\EncryptionDoctrineBridgeBundle\Entity\EncryptionDatabaseInterface;
use Farvest\EncryptionDoctrineBridgeBundle\Exceptions\EncryptionClassUndefinedException;
use ReflectionException;
use Symfony\Component\Form\FormInterface;

/** *****************************************************************************************************************
 *  Class EncryptionForm
 *  -----------------------------------------------------------------------------------------------------------------
 *  Class to add fields with decipher data on forms.
 *  -----------------------------------------------------------------------------------------------------------------
 *  @package Farvest\EncryptionDoctrineBridgeBundle\EncryptionDoctrineBridge
 *  ***************************************************************************************************************** */
class EncryptionForm
{
    /**
     *  @var Encryption
     *  ------------------------------------------------------------------------------------------------------------- */
    private $encryption;

    /** *************************************************************************************************************
     *  EncryptionForm constructor.
     *  -------------------------------------------------------------------------------------------------------------
     *  @param Encryption $encryption
     *  ************************************************************************************************************* */
    public function __construct(Encryption $encryption)
    {
        $this->encryption = $encryption;
    }

    /** *************************************************************************************************************
     *  @param FormInterface &$form
     *  @param EncryptionDatabaseInterface $entity
     *  @param string $type
     *  @param string $field
     *  @param array $options
     *  @throws EncryptionClassUndefinedException
     *  @throws ReflectionException
     *  ************************************************************************************************************* */
    public function addDecipherField(FormInterface &$form, EncryptionDatabaseInterface $entity, string  $type, string $field, array $options)
    {
        $property = 'get'.ucfirst($field);

        $options = [];
        if (null !== $entity->$property()) {
            $data = $this->encryption->decipherData(get_class($entity), $field, $entity->$property());
            $options['data'] = $data;
        }

        $form->add(
            $field,
            $type,
            $options
        );
    }
}