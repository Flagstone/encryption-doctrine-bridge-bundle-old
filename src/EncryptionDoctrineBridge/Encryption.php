<?php
/** *****************************************************************************************************************
 *  Encryption.php
 *  *****************************************************************************************************************
 *  @copyright 2019 Farvest
 *  @author Emmanuel Grosdemange <emmanuel.grosdemange@farvest.com>
 *  *****************************************************************************************************************
 *  Created: 2019/12/12
 ******************************************************************************************************************** */

namespace Farvest\EncryptionDoctrineBridgeBundle\EncryptionDoctrineBridge;

use Doctrine\Common\Annotations\Reader;
use Farvest\BaseEncoderBundle\Encoder\BaseEncoderInterface;
use Farvest\EncryptionDoctrineBridgeBundle\EncryptionDoctrineBridge\Encryption as Cipher;
use Farvest\EncryptionDoctrineBridgeBundle\Entity\EncryptionDatabaseInterface;
use Farvest\EncryptionDoctrineBridgeBundle\Exceptions\EncryptionClassUndefinedException;
use ReflectionException;
use ReflectionClass;


class Encryption
{
    /**
     *  @var Reader
     *  ------------------------------------------------------------------------------------------------------------- */
    private $annotationReader;

    /**
     *  @var EncryptionClassFactory
     *  ------------------------------------------------------------------------------------------------------------- */
    private $encryptionFactory;

    const ANNOTATION_CLASS = 'Farvest\EncryptionDoctrineBridgeBundle\Annotations\Encryption';
    const ENTITY_INTERFACE = 'Farvest\EncryptionDoctrineBridgeBundle\Entity\EncryptionDatabaseInterface';
    const CIPHER = 0;
    const DECIPHER = 1;

    /** *************************************************************************************************************
     *  Encryption constructor.
     *  -------------------------------------------------------------------------------------------------------------
     *  @param Reader $reader
     *  @param EncryptionClassFactory $encryptionFactory
     *  ************************************************************************************************************* */
    public function __construct(Reader $reader, EncryptionClassFactory $encryptionFactory)
    {
        $this->annotationReader = $reader;
        $this->encryptionFactory = $encryptionFactory;
    }

    /** *************************************************************************************************************
     *  Cipher an attribute of en entity
     *  -------------------------------------------------------------------------------------------------------------
     *  @param string $class
     *  @param string $attribute
     *  @param string $value
     *  @return string
     *  @throws EncryptionClassUndefinedException
     *  @throws ReflectionException
     *  ************************************************************************************************************* */
    public function cipherData(string $class, string $attribute, string $value)
    {
        return $this->cipher($class, $attribute, $value, self::CIPHER);
    }

    /** *************************************************************************************************************
     *  Decipher an attribute of en entity
     *  -------------------------------------------------------------------------------------------------------------
     *  @param string $class
     *  @param string $attribute
     *  @param string $value
     *  @return string
     *  @throws EncryptionClassUndefinedException
     *  @throws ReflectionException
     *  ************************************************************************************************************* */
    public function decipherData(string $class, string $attribute, string $value)
    {
        return $this->cipher($class, $attribute, $value, self::DECIPHER);
    }

    /** *************************************************************************************************************
     *  Cipher or decipher treatment. The class with @Encryption annotations must
     *  implement EncryptionDatabaseInterface.
     *  -------------------------------------------------------------------------------------------------------------
     *  @param string $class
     *  @param string $attribute
     *  @param string $value
     *  @param int $method
     *  @return string
     *  @throws EncryptionClassUndefinedException
     *  @throws ReflectionException
     *  ************************************************************************************************************* */
    public function cipher(string $class, string $attribute, string $value, int $method)
    {
        $reflectionClass = new ReflectionClass($class);
        if (in_array(self::ENTITY_INTERFACE, $reflectionClass->getInterfaceNames())) {
            $reflectionProperty = $reflectionClass->getProperty($attribute);
            $annotation = $this->annotationReader->getPropertyAnnotation($reflectionProperty, self::ANNOTATION_CLASS);
            if (null !== $annotation) {
                /**
                 *  @var BaseEncoderInterface
                 */
                $encodedClass = $this->encryptionFactory->build($annotation->getEncoderClass());
                if (self::CIPHER === $method) {
                    return $encodedClass->encode($value);
                }
                return $encodedClass->decode($value);
            }
        }
        return null;
    }

    /** *************************************************************************************************************
     *  Cipher all fields of an entity
     *  -------------------------------------------------------------------------------------------------------------
     *  @param $entity
     *  @return EncryptionDatabaseInterface
     *  @throws EncryptionClassUndefinedException
     *  @throws ReflectionException
     *  ************************************************************************************************************* */
    public function cipherAllFields($entity)
    {
        return $this->cipherFields($entity, self::CIPHER);
    }

    /** *************************************************************************************************************
     *  Decipher all fields of an entity
     *  -------------------------------------------------------------------------------------------------------------
     *  @param $entity
     *  @return EncryptionDatabaseInterface
     *  @throws EncryptionClassUndefinedException
     *  @throws ReflectionException
     *  ************************************************************************************************************* */
    public function decipherAllFields($entity)
    {
        return $this->cipherFields($entity, self::DECIPHER);
    }

    /** *************************************************************************************************************
     *  Cipher or decipher all fields with @Encryption annotation in an Entity. Rewrite all properties.
     *  -------------------------------------------------------------------------------------------------------------
     *  @param EncryptionDatabaseInterface $entity
     *  @param int $method
     *  @return EncryptionDatabaseInterface
     *  @throws EncryptionClassUndefinedException
     *  @throws ReflectionException
     *  ************************************************************************************************************* */
    private function cipherFields(EncryptionDatabaseInterface $entity, int $method): EncryptionDatabaseInterface
    {
        $entityClass = get_class($entity);
        $reflectedClass = new ReflectionClass($entityClass);
        $properties = $reflectedClass->getProperties();
        foreach ($properties as $property) {
            $annotation = $this->annotationReader->getPropertyAnnotation($property, Cipher::ANNOTATION_CLASS);
            if (null !==$annotation) {
                $getter = 'get' . ucfirst($property->getName());
                $setter = 'set' . ucfirst($property->getName());

                $encodedString = $this->cipher($entityClass, $property->getName(), $entity->$getter(), $method);
                if (null !== $encodedString) {
                    $entity->$setter($encodedString);
                }
            }
        }
        return $entity;
    }

    /** *************************************************************************************************************
     *  Return all encrypted field name for an entity
     *  -------------------------------------------------------------------------------------------------------------
     *  @param EncryptionDatabaseInterface $entity
     *  @return array
     *  @throws ReflectionException
     *  ************************************************************************************************************* */
    public function getEncryptedFieldsName(EncryptionDatabaseInterface $entity): array
    {
        $fieldsName = [];
        $entityClass = get_class($entity);
        $reflectedClass = new ReflectionClass($entityClass);
        $properties = $reflectedClass->getProperties();
        foreach ($properties as $property) {
            $annotation = $this->annotationReader->getPropertyAnnotation($property, Cipher::ANNOTATION_CLASS);
            if (null !== $annotation) {
                $fieldsName[] = $property->getName();
            }
        }
        return $fieldsName;
    }

    /** *************************************************************************************************************
     *  Test if an $entityBefore (encrypted) is equal to an $entityAfter(decrypted) after encrypted
     *  -------------------------------------------------------------------------------------------------------------
     *  @param string $valueBefore
     *  @param string $valueAfter
     *  @param string $class
     *  @param string $field
     *  @return bool
     *  @throws EncryptionClassUndefinedException
     *  @throws ReflectionException
     *  ************************************************************************************************************* */
    public function hasDataChanged(string $valueBefore, string $valueAfter, string $class, string $field)
    {
        return $valueBefore !== $this->cipher($class, $field, $valueAfter, Encryption::CIPHER);
    }
}