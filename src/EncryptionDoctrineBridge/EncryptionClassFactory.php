<?php
/** *****************************************************************************************************************
 *  EncryptionClassFactory.php
 *  -----------------------------------------------------------------------------------------------------------------
 *  @copyright 2019 Farvest
 *  @author Emmanuel Grosdemange <emmanuel.grosdemange@farvest.com>
 *  -----------------------------------------------------------------------------------------------------------------
 *  Created: 2019/12/11
 *  ***************************************************************************************************************** */

namespace Farvest\EncryptionDoctrineBridgeBundle\EncryptionDoctrineBridge;

use Farvest\BaseEncoderBundle\Encoder\BaseEncoderInterface;
use Farvest\EncryptionDoctrineBridgeBundle\Exceptions\EncryptionClassUndefinedException;

/** *****************************************************************************************************************
 *  Class EncryptionClassFactory
 *  -----------------------------------------------------------------------------------------------------------------
 *  Create the encryption class to use based on the class name
 *  -----------------------------------------------------------------------------------------------------------------
 *  @package Farvest\EncrytionDoctrineBridgeBundle
 *  ***************************************************************************************************************** */
class EncryptionClassFactory
{
    /** -------------------------------------------------------------------------------------------------------------
     *  @var string
     *  ------------------------------------------------------------------------------------------------------------- */
    private $defaultEncoder;

    /** *************************************************************************************************************
     *  EncryptionClassFactory constructor.
     *  -------------------------------------------------------------------------------------------------------------
     *  @param string $defaultEncoder
     *  @throws EncryptionClassUndefinedException
     *  ************************************************************************************************************* */
    public function __construct(string $defaultEncoder)
    {
        $this->defaultEncoder = $defaultEncoder;

        if (!class_exists($defaultEncoder)) {
            $this->encryptionClassNotExists($defaultEncoder);
        }
    }

    /** *************************************************************************************************************
     *  Return the encryption class defined by the class name. If class name is null, return the default class
     *  defined in the bundle parameters.
     *  -------------------------------------------------------------------------------------------------------------
     *  @param string|null $encryptClass
     *  @return BaseEncoderInterface
     *  @throws EncryptionClassUndefinedException
     *  ************************************************************************************************************* */
    public function build(?string $encryptClass): BaseEncoderInterface
    {
        if (null === $encryptClass) {
           return new $this->defaultEncoder();
        }

        if (class_exists($encryptClass)) {
            return new $encryptClass();
        }

        $this->encryptionClassNotExists($encryptClass);
    }

    /** *************************************************************************************************************
     *  Thrown an exception when the encryption class doesn't exist.
     *  -------------------------------------------------------------------------------------------------------------
     *  @param $class
     *  @throws EncryptionClassUndefinedException
     *  ************************************************************************************************************* */
    public function encryptionClassNotExists($class)
    {
        throw new EncryptionClassUndefinedException(sprintf('The encoder class \'%s\' doesn\'t exist.', $class));
    }
}