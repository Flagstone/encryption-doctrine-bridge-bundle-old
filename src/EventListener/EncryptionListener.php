<?php
/** *****************************************************************************************************************
 *  EncryptionListener.php
 *  *****************************************************************************************************************
 *  @copyright 2019 Farvest
 *  @author Emmanuel Grosdemange <emmanuel.grosdemange@farvest.com>
 *  *****************************************************************************************************************
 *  Created: 2019/12/09
 *  ***************************************************************************************************************** */

namespace Farvest\EncryptionDoctrineBridgeBundle\EventListener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Farvest\EncryptionDoctrineBridgeBundle\EncryptionDoctrineBridge\Encryption;
use Farvest\EncryptionDoctrineBridgeBundle\Entity\EncryptionDatabaseInterface;
use ReflectionException;
use Farvest\EncryptionDoctrineBridgeBundle\Exceptions\EncryptionClassUndefinedException;

/** *****************************************************************************************************************
 *  Class EncryptionListener
 *  -----------------------------------------------------------------------------------------------------------------
 *  Listener for encrypt data on an entity.
 *  -----------------------------------------------------------------------------------------------------------------
 *  @package Farvest\EncryptionDoctrineBridgeBundle\EventListener
 *  ***************************************************************************************************************** */
class EncryptionListener
{
    /**
     *  @var Encryption
     *  ------------------------------------------------------------------------------------------------------------- */
    private $encryption;

    /** *************************************************************************************************************
     *  EncryptionListener constructor.
     *  -------------------------------------------------------------------------------------------------------------
     *  @param Encryption $encryption
     *  ************************************************************************************************************* */
    public function __construct(Encryption $encryption)
    {
        $this->encryption = $encryption;
    }

    /** *************************************************************************************************************
     *  Operation to do before entities with EncryptionDatabaseInterface Interface is updated
     *  Operation is made only if isEncryptionListenerEnabled is set to TRUE, to avoid multiple calls
     *  -------------------------------------------------------------------------------------------------------------
     *  @param LifecycleEventArgs $event
     *  @throws EncryptionClassUndefinedException
     *  @throws ReflectionException
     *  ************************************************************************************************************* */
    public function preUpdate(LifecycleEventArgs $event)
    {
        if (!$event->getEntity() instanceof EncryptionDatabaseInterface) {
            return;
        }
        /**
         * @var EncryptionDatabaseInterface
         */
        $entity = $event->getEntity();
        if (true === $entity->isEncryptionListenerEnabled()) {
            $this->encryptFields($entity);
        }
    }

    /** *************************************************************************************************************
     *  Operation to do before entities with EncryptionDatabaseInterface Interface is created
     *  Operation is made only if isEncryptionListenerEnabled is set to TRUE, to avoid multiple calls
     *  -------------------------------------------------------------------------------------------------------------
     *  @param LifecycleEventArgs $event
     *  @throws EncryptionClassUndefinedException
     *  @throws ReflectionException
     *  ************************************************************************************************************* */
    public function prePersist(LifecycleEventArgs $event)
    {
        if (!$event->getEntity() instanceof EncryptionDatabaseInterface) {
            return;
        }
        /**
         * @var EncryptionDatabaseInterface
         */
        $entity = $event->getEntity();
        if (true === $entity->isEncryptionListenerEnabled()) {
            $this->encryptFields($entity);
        }
    }

    /** *************************************************************************************************************
     *  Make the encryption.
     *  -------------------------------------------------------------------------------------------------------------
     *  @param $entity
     *  @throws EncryptionClassUndefinedException
     *  @throws ReflectionException
     *  ************************************************************************************************************* */
    private function encryptFields($entity)
    {
        $entity = $this->encryption->cipherAllFields($entity);
    }
}
