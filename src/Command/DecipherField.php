<?php
/** *****************************************************************************************************************
 *  DecipherField.php
 *  *****************************************************************************************************************
 *  @copyright 2019 Farvest
 *  @author Emmanuel Grosdemange <emmanuel.grosdemange@farvest.com>
 *  *****************************************************************************************************************
 *  Created: 2019/12/20
 ******************************************************************************************************************** */

namespace Farvest\EncryptionDoctrineBridgeBundle\Command;

use Farvest\EncryptionDoctrineBridgeBundle\EncryptionDoctrineBridge\Encryption as Cipher;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Farvest\EncryptionDoctrineBridgeBundle\Exceptions\EncryptionClassUndefinedException;
use ReflectionException;

/** *****************************************************************************************************************
 *  Class DecipherField
 *  -----------------------------------------------------------------------------------------------------------------
 *  Command to decrypt a field on all entities
 *  -----------------------------------------------------------------------------------------------------------------
 *  @package Farvest\EncryptionDoctrineBridgeBundle\Command
 *  ***************************************************************************************************************** */
class DecipherField extends Command
{
    /**
     *  @var string
     *  ------------------------------------------------------------------------------------------------------------- */
    protected static $defaultName = 'fv-encrypt:field:decipher';

    /**
     *  @var Encryption
     *  ------------------------------------------------------------------------------------------------------------- */
    private $encryption;

    /** *************************************************************************************************************
     *  CipherField constructor.
     *  -------------------------------------------------------------------------------------------------------------
     * @param Encryption $encryption
     *  ************************************************************************************************************* */
    public function __construct(Encryption $encryption)
    {
        parent::__construct();
        $this->encryption = $encryption;
    }

    /** *************************************************************************************************************
     *  Configuration of the command
     *  ************************************************************************************************************* */
    protected function configure()
    {
        $this
            ->setDescription('Decipher all data defined by its entity and a field.')
            ->setHelp('/!\ Be sure to decipher cipher data. You can loose your data if this command is not correctly used.');
    }

    /** *************************************************************************************************************
     *  Execution of the command.
     *  -------------------------------------------------------------------------------------------------------------
     *  @param InputInterface $input
     *  @param OutputInterface $output
     *  @return int|void
     *  @throws EncryptionClassUndefinedException
     *  @throws ReflectionException
     *  ************************************************************************************************************* */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->write(sprintf("\033\143"));
        $output->writeln([
            '',
            '<fg=green>Farvest Encryption Doctrine Bridge - DecipherField</>',
            '<fg=yellow>==================================================</>',
            '',
            '<fg=green>Decrypt all database data defined by its entity and its field.</>',
            '<fg=green>After decrypt a field, you must remove @Encryption annotation on the entity you want to decrypt.</>',
            '',
            '<fg=red>/!\ Be sure to decipher cipher data. You can loose your data if this command is not correctly used.</>',
            '<fg=red>You can encrypt data by using the cipher command (make sure that base and string are not changed).</>',
            '<fg=yellow>=====================================================================================================</>',
        ]);

        $entity = CommandUtils::askQuestion(
            'Entity full name concern by the decryption',
            true,
            'Entity is required'
        );

        $field = CommandUtils::askQuestion(
            'Property of the entity concern by the decryption',
            true,
            'Property is required'
        );

        $this->encryption->encryptionField($entity, $field, Cipher::DECIPHER);
    }
}