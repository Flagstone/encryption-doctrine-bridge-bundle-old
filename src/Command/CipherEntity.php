<?php
/** *****************************************************************************************************************
 *  CipherEntity.php
 *  *****************************************************************************************************************
 *  @copyright 2019 Farvest
 *  @author Emmanuel Grosdemange <emmanuel.grosdemange@farvest.com>
 *  *****************************************************************************************************************
 *  Created: 2019/01/09
 ******************************************************************************************************************** */

namespace Farvest\EncryptionDoctrineBridgeBundle\Command;

use Farvest\EncryptionDoctrineBridgeBundle\EncryptionDoctrineBridge\Encryption as Cipher;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Farvest\EncryptionDoctrineBridgeBundle\Exceptions\EncryptionClassUndefinedException;
use ReflectionException;

/** *****************************************************************************************************************
 *  Class CipherEntity
 *  -----------------------------------------------------------------------------------------------------------------
 *  Command to encrypt all fields that have @Encryption annotation on all entities
 *  -----------------------------------------------------------------------------------------------------------------
 *  @package Farvest\EncryptionDoctrineBridgeBundle\Command
 *  ***************************************************************************************************************** */
class CipherEntity extends Command
{
    /**
     *  @var string
     *  ------------------------------------------------------------------------------------------------------------- */
    protected static $defaultName = 'fv-encrypt:entity:cipher';

    /**
     *  @var Encryption
     *  ------------------------------------------------------------------------------------------------------------- */
    private $encryption;

    /** *************************************************************************************************************
     *  CipherField constructor.
     *  -------------------------------------------------------------------------------------------------------------
     * @param Encryption $encryption
     *  ************************************************************************************************************* */
    public function __construct(Encryption $encryption)
    {
        parent::__construct();
        $this->encryption = $encryption;
    }

    /** *************************************************************************************************************
     *  Configuration of the command
     *  ************************************************************************************************************* */
    protected function configure()
    {
        $this
            ->setDescription('Cipher all data defined by its entity.')
            ->setHelp('/!\ Be sure to cipher non-cipher data. You can loose your data if this command is not correctly used.');
    }

    /** *************************************************************************************************************
     *  Execution of the command.
     *  -------------------------------------------------------------------------------------------------------------
     *  @param InputInterface $input
     *  @param OutputInterface $output
     *  @return int|void
     *  @throws EncryptionClassUndefinedException
     *  @throws ReflectionException
     *  ************************************************************************************************************* */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->write(sprintf("\033\143"));
        $output->writeln([
            '',
            '<fg=green>Farvest Encryption Doctrine Bridge - Cipher Entity</>',
            '<fg=yellow>=================================================</>',
            '',
            '<fg=green>Encrypt all database data defined by its entity.</>',
            '<fg=green>Before encrypt an entity, you must add @Encryption annotation on all fields of the entity you want to encrypt.</>',
            '',
            '<fg=red>/!\ Be sure to cipher non-cipher data. You can loose your data if this command is not correctly used.</>',
            '<fg=red>You can decrypt data by using the decipher command (make sure that base and string are not changed).</>',
            '<fg=yellow>=====================================================================================================</>',
        ]);

        $entity = CommandUtils::askQuestion(
            'Entity full name concern by the encryption',
            true,
            'Entity is required'
        );

        $this->encryption->encryptionEntity($entity, Cipher::CIPHER);
    }
}