<?php
/** *****************************************************************************************************************
 *  EncryptionDatabaseInterface.php
 *  *****************************************************************************************************************
 *  @copyright 2019 Farvest
 *  @author Emmanuel Grosdemange <emmanuel.grosdemange@farvest.com>
 *  *****************************************************************************************************************
 *  Created: 2019/12/09
 ******************************************************************************************************************** */

namespace Farvest\EncryptionDoctrineBridgeBundle\Entity;

/** *****************************************************************************************************************
 *  Interface EncryptionDatabaseInterface
 *  -----------------------------------------------------------------------------------------------------------------
 *  Interface for entities that want to use Encryption capabilities, and manage data persistence (Listener).
 *  The two methods have to be implements to avoid (or not) the EncryptionListener.
 *  -----------------------------------------------------------------------------------------------------------------
 *  @package Farvest\EncrytionDoctrineBridge\Entity
 *  ***************************************************************************************************************** */
interface EncryptionDatabaseInterface
{
    public function isEncryptionListenerEnabled(): bool;
    public function setEncryptionListenerEnabled(bool $encryptionListenerEnabled);
}
