<?php
/** *****************************************************************************************************************
 *  EncryptionDoctrineExtension.php
 *  *****************************************************************************************************************
 *  @copyright 2019 Farvest
 *  @author Emmanuel Grosdemange <emmanuel.grosdemange@farvest.com>
 *  *****************************************************************************************************************
 *  Created: 2019/12/13
 ******************************************************************************************************************** */

namespace Farvest\EncryptionDoctrineBridgeBundle\Twig;

use Farvest\EncryptionDoctrineBridgeBundle\EncryptionDoctrineBridge\Encryption;
use Farvest\EncryptionDoctrineBridgeBundle\Entity\EncryptionDatabaseInterface;
use Farvest\EncryptionDoctrineBridgeBundle\Exceptions\EncryptionClassUndefinedException;
use ReflectionException;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

/** *****************************************************************************************************************
 *  Class EncryptionDoctrineExtension
 *  -----------------------------------------------------------------------------------------------------------------
 *  Twig extension to decipher string on twig templates.
 *  -----------------------------------------------------------------------------------------------------------------
 *  @package Farvest\EncryptionDoctrineBridgeBundle\Twig
 *  ***************************************************************************************************************** */
class EncryptionDoctrineExtension extends AbstractExtension
{
    /**
     *  @var Encryption
     *  ------------------------------------------------------------------------------------------------------------- */
    private $encryption;

    /** *************************************************************************************************************
     *  EncryptionDoctrineExtension constructor.
     *  ------------------------------------------------------------------------------------------------------------
     *  @param Encryption $encryption
     *  ************************************************************************************************************* */
    public function __construct(Encryption $encryption)
    {
        $this->encryption = $encryption;
    }

    /** *************************************************************************************************************
     *  Attach bundle's filters to all twig filters
     *  -------------------------------------------------------------------------------------------------------------
     *  @return array|TwigFilter[]
     *  ************************************************************************************************************* */
    public function getFilters()
    {
        return [
            new TwigFilter('decipher', [$this, 'decipher'])
        ];
    }

    /** *************************************************************************************************************
     *  @param string $value
     *  @param EncryptionDatabaseInterface $entity
     *  @param string $property
     *  @return string
     *  @throws EncryptionClassUndefinedException
     *  @throws ReflectionException
     *  ************************************************************************************************************* */
    public function decipher(string $value, EncryptionDatabaseInterface $entity, string $property)
    {
        return $this->encryption->decipherData(get_class($entity), $property, $value);
    }
}
